package com.example.hbb.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.hbb.model.po.ActivityComment;
import com.example.hbb.model.vo.CommentVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author Liululin
 * @date 2024/1/2 - 14:13
 */
@Mapper
public interface ActivityCommentMapper extends BaseMapper<ActivityComment> {
    @Select("select * from activity_comment")
    List<ActivityComment> findAll();
}
