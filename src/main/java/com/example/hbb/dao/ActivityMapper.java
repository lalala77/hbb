package com.example.hbb.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.hbb.model.po.Activity;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author Liululin
 * @date 2024/1/2 - 14:11
 */
@Mapper
public interface ActivityMapper extends BaseMapper<Activity> {
    @Select("select * from activity")
    List<Activity> findAll();
}
