package com.example.hbb.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.hbb.model.po.ActivityLabel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author Liululin
 * @date 2024/1/2 - 14:18
 */
@Mapper
public interface ActivityLabelMapper extends BaseMapper<ActivityLabel> {
    @Select("select * from activity_label")
    List<ActivityLabel> findAll();
}
