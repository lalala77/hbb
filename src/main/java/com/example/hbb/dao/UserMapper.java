package com.example.hbb.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.hbb.model.po.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.ArrayList;

/**
 * @author Liululin
 * @date 2024/1/2 - 14:19
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
//    public User selectBySid(String sid);
    @Select("select id from user")
    ArrayList<Integer> getAllIds();
}
