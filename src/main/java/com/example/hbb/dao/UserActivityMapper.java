package com.example.hbb.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.hbb.model.po.UserActivity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Liululin
 * @date 2024/1/2 - 14:19
 */
@Mapper
public interface UserActivityMapper extends BaseMapper<UserActivity> {
}
