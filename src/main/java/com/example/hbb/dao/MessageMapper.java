package com.example.hbb.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.hbb.model.po.Message;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MessageMapper extends BaseMapper<Message> {
}
