package com.example.hbb.model;

public enum MessageTypes {
    ACTIVITY_DUE("活动开始提醒"),
    PUBLISH_SUCCESS("活动审核通过"),
    PUBLISH_FAIL("活动审核未通过");

    private final String type;

    MessageTypes(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
