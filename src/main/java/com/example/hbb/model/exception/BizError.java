package com.example.hbb.model.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.lang.model.type.ErrorType;

@AllArgsConstructor
@Getter
public enum BizError{

    USER_NOEXISTS(200001, "用户不存在", 400),
    INVALID_CREDENTIAL(200002, "用户名或密码错误", 400),
    STATIONNAME_EXISTS(200003, "同名站点已存在", 400),
    USER_EXISTS(200004, "用户已存在", 400);

//    INSUFFICIENT_BALANCE(400002,"账户余额不足",400);

    final int code;
    final String message;
    final int httpCode;
}
