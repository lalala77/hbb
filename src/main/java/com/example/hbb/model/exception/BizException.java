package com.example.hbb.model.exception;


/**
 * 自定义业务异常类
 */
public class BizException extends RuntimeException {
    public BizException(BizError bizError) {
        super(bizError.message); // 调用父类的构造方法，传入错误信息
    }
}
