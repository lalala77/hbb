package com.example.hbb.model;

/**
 * @author Liululin
 * @date 2024/1/2 - 14:29
 */
public enum Label {
    MUSIC("音乐"),
    SPORTS("运动"),
    ACG("动漫"),
    LECTURE("讲座"),
    GAME("游戏"),
    TABLE_GAME("桌游"),
    MEAL("吃饭"),
    VOLUNTEER("志愿活动"),
    OTHERS("其他"),
    TOGETHER("团建");


    private final String label;
    Label(String label) {
        this.label = label;
    }
    public String getLabel() {
        return label;
    }
}
