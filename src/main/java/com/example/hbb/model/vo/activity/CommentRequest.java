package com.example.hbb.model.vo.activity;

import lombok.Data;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.util.Date;

@Data
public class CommentRequest {
    @NotNull
    private Integer aid; //评论的活动id

    @NotNull
    private String content;

    @NotNull
    private Integer uid;

    @NotNull
    @PastOrPresent
    private Date time;
}
