package com.example.hbb.model.vo.user;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class LoginRequest {
    @NotNull
    @Pattern(regexp = "^[0-9]+$",message = "请输入学号")
    private String userId; // 学号

    @NotNull
    private String password; //用户密码
}
