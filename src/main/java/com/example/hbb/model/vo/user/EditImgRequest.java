package com.example.hbb.model.vo.user;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class EditImgRequest {
    @NotNull
    private Integer Id;

    @NotNull
    private String avatarUrl;  //todo 这里也许需要进一步校验URL的格式
}
