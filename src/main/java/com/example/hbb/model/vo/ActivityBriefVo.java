package com.example.hbb.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
/*
  这个类是展示首页中的每一个活动的简要信息
 */
public class ActivityBriefVo {
    /**
     * 活动id
     */
    private Integer id;
    /**
     * 活动名称
     */
    private String name;
    /**
     * 活动图片
     */
    private String img;
    /**
     * 发布者名称
     */
    private String username;
    /**
     * 发布者头像
     */
    private String userImg;
    /**
     * 已报名人数
     */
    private Integer signedNumber;
    /**
     * 报名最大人数
     */
    private Integer maxNumber;
    /**
     * 活动状态  0 未开始报名 1 正在报名  2 报名结束  3 活动进行中 4 活动已结束
     */
    private String process;

    /**
     * 活动点赞数
     */
    private Integer praisedNumber;

}
