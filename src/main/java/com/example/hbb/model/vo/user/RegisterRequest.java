package com.example.hbb.model.vo.user;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class RegisterRequest {
    @NotNull
    @Pattern(regexp = "^[0-9]+$",message = "请输入学号")
    private String userId; // 学号

    @NotNull
    private String password; //用户密码

    @NotNull
    @Size(min = 2,max = 6, message = "用户昵称需要在2~6之间")
    private String name;

    @NotNull
    private String avatarUrl;
}
