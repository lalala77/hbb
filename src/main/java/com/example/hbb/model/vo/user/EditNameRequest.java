package com.example.hbb.model.vo.user;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class EditNameRequest {
    @NotNull
    private Integer Id;

    @NotNull
    @Size(min = 2,max = 6, message = "用户昵称需要在2~6之间")
    private String name;
}
