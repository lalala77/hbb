package com.example.hbb.model.vo;

import com.example.hbb.model.MessageTypes;
import com.example.hbb.model.po.Message;
import lombok.Data;

import javax.persistence.Id;

@Data
public class MessageVo {

    //消息id
    @Id
    private Integer id;

    /**
     * 消息标题 -> 对应消息类型: 活动开始提醒、审核结果提示……
     */
    private String title;
    /**
     * 消息内容
     */
    private String content;

    //消息生成的时间
    private String date;

    public MessageVo(Message message){
        id = message.getId();
        MessageTypes messageType = MessageTypes.valueOf(message.getType());
        switch (messageType) {
            case ACTIVITY_DUE -> content = "您报名的活动\"" + message.getContent() + "\"快开始了，别忘了参加哦";
            case PUBLISH_SUCCESS -> content = "恭喜!您之前发布的活动\"" + message.getContent() + "\"已通过审核";
            case PUBLISH_FAIL -> {
                String[] messageInfo = message.getContent().split("&");
                content = "很抱歉，您之前发布的活动\"" + messageInfo[0] + "\"未能通过审核\n" + "审核意见如下: \n" + messageInfo[1];
            }
            default -> {
            }
        }
        title = messageType.getType();
        date = message.getDate();
    }

}
