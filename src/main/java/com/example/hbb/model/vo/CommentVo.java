package com.example.hbb.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Liululin
 * @date 2024/1/2 - 14:31
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentVo {
    /**
     * 评论内容
     */
    private String content;
    /**
     * 用户id
     */
    private Integer uid;
    /**
     * 评论时间
     */
    private Date time;
}
