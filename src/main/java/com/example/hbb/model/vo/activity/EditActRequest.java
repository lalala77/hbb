package com.example.hbb.model.vo.activity;

import lombok.Data;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;

@Data
public class EditActRequest {
    @NotNull
    private Integer id; //唯一的活动id

    @NotNull
    @Size(min = 1,max = 20,message = "活动名在1~20字符以内")
    private String name;

    @NotNull
    @Size(min = 1, max = 200,message = "简介应在200字以内")
    private String introduction;

    @NotNull
    private String img;

    @NotNull
    @FutureOrPresent
    private Date startTime;

    @NotNull
    @FutureOrPresent
    private Date endTime;

    @NotNull
    @FutureOrPresent
    private Date startSign;

    @NotNull
    @FutureOrPresent
    private Date endSign;

    @NotNull
    @Positive(message = "必须为正数")
    private Integer maxNumber;

    private ArrayList<String> labels;

}
