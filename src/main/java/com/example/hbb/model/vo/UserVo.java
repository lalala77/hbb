package com.example.hbb.model.vo;

import com.example.hbb.model.po.User;
import lombok.Data;


import javax.persistence.Id;
import java.util.ArrayList;
import java.util.Map;
/**
 * @author Liululin
 * @date 2024/1/2 - 10:31
 */
@Data
public class UserVo {
    /**
     * 用户id
     */
    @Id
    private Integer id;
    /**
     * 学号
     */
    private String sid;
    /**
     * 用户名
     */
    private String name;

    /**
     * 用户头像
     */
    private String avatarUrl;

    //收藏的活动，存的是活动id
    private ArrayList<Integer> collectActs;

    //报名的活动
    private ArrayList<Integer> signUpActs ;

    //发布的活动 hashmap<activityId, 活动状态status>
    private Map<String, String> publishActs;

//    //收到的消息
//    private ArrayList<MessageVo> messages;

    public UserVo(User user){
        this.id = user.getId();
        this.name = user.getName();
        this.sid = user.getSid();
        this.avatarUrl = user.getAvatarUrl();
    }

}
