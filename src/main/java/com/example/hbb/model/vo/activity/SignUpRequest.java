package com.example.hbb.model.vo.activity;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SignUpRequest {
    @NotNull
    private Integer uid;

    @NotNull
    private Integer actId;
}
