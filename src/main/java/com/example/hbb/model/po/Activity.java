package com.example.hbb.model.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * @author Liululin
 * @date 2024/1/2 - 10:32
 */
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Activity {
    /**
     * 活动id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /**
     * 活动名称
     */
    private String name;

    /**
     * 活动简介
     */
    private String introduction;

    /**
     * 发布者id
     */
    private Integer uid;

    /**
     * 活动图片
     */
    private String img;

    /**
     * 活动开始时间
     */
    private Date startTime;

    /**
     * 活动结束时间
     */
    private Date endTime;

    /**
     * 活动报名开始时间
     */
    private Date startSign;

    /**
     * 活动报名结束时间
     */
    private Date endSign;

    /**
     * 活动状态 0 未开始报名 1 正在报名  2 报名结束  3 活动进行中 4 活动已结束
     */
    private Integer status;

    /**
     * 最大报名人数
     */
    private Integer maxNumber;

    /**
     * 已报名人数
     */
    private Integer signedNumber;


    /**
     * 活动点赞数
     */
    private Integer praisedNumber;

}
