package com.example.hbb.model.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;
/**
 * @author Liululin
 * @date 2024/1/2 - 13:08
 */

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ActivityComment {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /**
     * 活动id
     */
    private Integer aid;
    /**
     * 评论内容
     */
    private String content;
    /**
     * 用户id
     */
    private Integer uid;
    /**
     * 评论时间
     */
    private  Date time;
}
