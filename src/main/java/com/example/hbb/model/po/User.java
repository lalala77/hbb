package com.example.hbb.model.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
/**
 * @author Liululin
 * @date 2024/1/2 - 10:32
 */
public class User {
    /**
     * 用户id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /**
     * 学号
     */

    private String sid;
    /**
     * 用户名
     */
    private String name;

    /**
     * 用户密码
     */
    @NotNull
    private String password;
    /**
     * 用户头像
     */

    private String avatarUrl;

}
