package com.example.hbb.model.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Liululin
 * @date 2024/1/2 - 13:05
 */

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActivityLabel {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /**
     * 活动id
     */
    private Integer aid;
    /**
     * 对应的标签
     */
    private String label;

}
