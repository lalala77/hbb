package com.example.hbb.model.po;

/**
 * @author Liululin
 * @date 2024/1/2 - 13:02
 */

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author Liululin
 * @date 2024/1/2 - 10:32
 */

@Entity
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class UserActivity {
    /**
     * 主键
     */
    @Id
    @TableId
    private Integer id;
    /**
     * 用户id
     */
    private Integer uid;
    /**
     * 活动id
     */
    private Integer aid;
    /**
     * 普通用户视角下活动的状态 用String类型存储enum.toString
     */
    private String status;

}
