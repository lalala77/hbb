package com.example.hbb.model.po;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /**
     * 用户id
     */
    private Integer uid;
    /**
     * 消息类型: 活动开始提醒、审核结果提示……
     */
    private String type;
    /**
     * 消息内容
     */
    private String content;

    //消息产生的时间
    private String date;
}
