package com.example.hbb.model;

/**
 * @author Liululin
 * @date 2024/1/2 - 14:25
 */
public enum ActivityStatus {
    IN_REVIEW("待审核"),
    SUCCESS_REVIEW("审核通过"),
    FAILD_REVIEW("审核未通过"),
    SIGNED_UP("已报名"),
    FAVORITE("已收藏");



    private final String status;

    ActivityStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
