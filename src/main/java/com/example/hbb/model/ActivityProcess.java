package com.example.hbb.model;

/**
 * @author Liululin
 * @date 2024/1/12 - 10:40
 */
public enum ActivityProcess {
    NOT_STARTED("未开始报名"),
    REGISTRATION_OPEN("正在报名"),
    REGISTRATION_CLOSED("报名截止"),
    ONGOING("活动进行中"),
    FINISHED("活动结束");

    private final String process;
    ActivityProcess( String process) {
        this.process = process;
    }
    public String getProcess() {
        return process;
    }



}
