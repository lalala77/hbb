package com.example.hbb.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;

@Service
public interface PublishService {
    /**
     * 用户发布活动
     * @param uid:发布者id
     * @param name: 活动名称
     * @param introduction: 活动简介
     * @param startTime: 活动开始时间，date格式
     * @param endTime: 活动结束时间
     * @param startSign: 报名开始时间
     * @param endSign: 报名结束时间
     * @param maxNumber:最大可报名人数
     * @return 返回当前活动的id，为了之后往活动-标签表中添加内容
     */
    int publishActivity(Integer uid, String name, String introduction,String img, Date startTime,
                         Date endTime, Date startSign, Date endSign, Integer maxNumber);

    /**
     * 在活动-标签表中添加tags
     * @param id:活动id
     * @param labels:多个标签
     */
    void addActivityLabel(Integer id, ArrayList<String> labels);

    /**
     * 修改内容，因此有些是维持不变的，不需要额外添加id，由于更改者是同一个人，所以不需要传递userId参数了
     */
    void editActivity(Integer id, String name, String introduction, String img,Date startTime,
                      Date endTime, Date startSign, Date endSign, Integer maxNumber);

}
