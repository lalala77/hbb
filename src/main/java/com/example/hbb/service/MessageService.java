package com.example.hbb.service;

import com.example.hbb.model.MessageTypes;
import com.example.hbb.model.vo.MessageVo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public interface MessageService {

    public void judgeDue(Integer uid, ArrayList<Integer> activities);

    public void addMessage(Integer uid, MessageTypes type, String content);

    public ArrayList<MessageVo> getAllMessages(int userId);

}
