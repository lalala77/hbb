package com.example.hbb.service;

import org.springframework.stereotype.Service;

/**
 * @author Liululin
 * @date 2024/1/14 - 7:44
 */
@Service
public interface OpenAIService {
        String getFeedback(Integer uid, String title,String introduction);
}
