package com.example.hbb.service;

import com.example.hbb.model.vo.UserVo;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;


public interface UserService {
    /**
     * 实现用户的登录
     * @param sId：用户学号
     * @param password：用户密码
     */
    Integer login(String sId, String password);

    /**
     * 修改用户的昵称
     * @param Id：用户ID
     * @param newName：用户新的名字
     */
    void editUsername(Integer Id, String newName);

    /**
     * 修改用户头像
     * @param Id：用户ID
     * @param newAvatarUrl：统一资源符
     */
    void editUserImg(Integer Id, String newAvatarUrl);


    /**
     * 返回UserVo对象
     */
    public UserVo getUserVo(Integer Id);

    public void addUser(String sid, String userName, String password, String avatarUrl);

    public ArrayList<Integer> getAllUsers();
}
