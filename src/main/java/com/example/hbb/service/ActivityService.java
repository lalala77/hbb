package com.example.hbb.service;

import com.example.hbb.model.vo.ActivityBriefVo;
import com.example.hbb.model.vo.ActivityVo;
import com.example.hbb.model.vo.CommentVo;


import java.util.Date;
import java.util.List;

public interface ActivityService {
    /**
     * 用户报名活动
     * @param uid:用户id
     * @param actId:所报名的活动id
     */
    void signUpActivity(Integer uid, Integer actId);

    /**
     * 用户收藏活动
     * @param uid:用户id
     * @param aid:活动id
     */
    void collectActivity(Integer uid, Integer aid);

    /**
     * 用户评论活动
     * @param aid：活动id
     * @param content:评论内容
     * @param uid:用户id
     * @param time：评论时间
     */
    void commentActivity(Integer aid, String content, Integer uid, Date time);

    /**
     * 首页请求展示活动内容后返回内容，与活动信息表有关
     * @param showStartNum:当前从数据库第几个开始返回
     * @return 活动列表
     */
    List<ActivityBriefVo> showAllActivity(int showStartNum);

    /**
     * 用户点击详情页面用于查看Activity详情，因此需要包含活动信息表内容和用户-活动表
     * @param aid:活动具体的id
     * @return 返回具体活动信息
     */
     ActivityVo showDetailActivity(Integer aid);

     List<CommentVo> showComment(Integer aid);

    /**
     * 用户输入搜索信息，查找符合用户搜索的活动
     * @param searchInfo:用户输入的搜索信息
     * @return
     */
     List<ActivityVo> search(String searchInfo);

     //仅测试用
     public void delete(Integer id);

}
