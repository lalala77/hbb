package com.example.hbb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.hbb.dao.*;
import com.example.hbb.model.ActivityProcess;
import com.example.hbb.model.ActivityStatus;
import com.example.hbb.model.po.*;
import com.example.hbb.model.vo.ActivityBriefVo;
import com.example.hbb.model.vo.ActivityVo;
import com.example.hbb.model.vo.CommentVo;
import com.example.hbb.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ActivityServiceImpl implements ActivityService {
    @Autowired
    UserActivityMapper userActivityMapper;
    @Autowired
    ActivityCommentMapper activityCommentMapper;
    @Autowired
    ActivityMapper activityMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    ActivityLabelMapper activityLabelMapper;

    public ActivityVo convertVo(Activity activity){
        ActivityVo activityVo = new ActivityVo();
        activityVo.setId(activity.getId());
        activityVo.setName(activity.getName());
        activityVo.setImg(activity.getImg());
        activityVo.setUid(activity.getUid());
        activityVo.setIntroduction(activity.getIntroduction());
        activityVo.setStartTime(activity.getStartTime());
        activityVo.setEndTime(activity.getEndTime());
        activityVo.setStartSign(activity.getStartSign());
        activityVo.setEndSign(activity.getEndSign());
        activityVo.setMaxNumber(activity.getMaxNumber());
        activityVo.setStatus(activity.getStatus());
        activityVo.setSignedNumber(activity.getSignedNumber());
        activityVo.setPraisedNumber(activity.getPraisedNumber());

        return activityVo;
    }

    /**
     * 用户报名活动
     * @param uid:用户id
     * @param actId:所报名的活动id
     */
    @Override
    public void signUpActivity(Integer uid, Integer actId) {
        //需要判断一下数据库中，是否存在对应的数据
        //where条件
        QueryWrapper<UserActivity> userActivityQueryWrapper = new QueryWrapper<>();
        userActivityQueryWrapper.eq("uid", uid).eq("aid", actId).eq("status", ActivityStatus.SIGNED_UP.toString());
        QueryWrapper<Activity> activityQueryWrapper = new QueryWrapper<>();
        UserActivity activities = userActivityMapper.selectOne(userActivityQueryWrapper);
        Activity activity = activityMapper.selectById(actId);
        if(activities != null){
            //说明已经报名了，就取消报名
            userActivityMapper.deleteById(activities.getId());
            int curSigned = activity.getSignedNumber();
            activity.setSignedNumber(curSigned-1);
            activityMapper.updateById(activity);
        }else {
            //否则，就新增一项报名的活动
            UserActivity userActivity = UserActivity.builder().uid(uid).aid(actId).status(ActivityStatus.SIGNED_UP.toString()).build();
            userActivityMapper.insert(userActivity);
            int curSigned = activity.getSignedNumber();
            activity.setSignedNumber(curSigned+1);
            activityMapper.updateById(activity);

        }
        activityMapper.updateById(activity);
    }

    /**
     * 用户收藏活动
     * @param uid:用户id
     * @param aid:活动id
     */
    public void collectActivity(Integer uid, Integer aid){
        //需要判断一下数据库中，是否存在对应的数据
        //where条件
        QueryWrapper<UserActivity> userActivityQueryWrapper = new QueryWrapper<>();
        userActivityQueryWrapper.eq("uid", uid).eq("aid",aid).eq("status", ActivityStatus.FAVORITE.toString());
        UserActivity activities = userActivityMapper.selectOne(userActivityQueryWrapper);
        Activity activity = activityMapper.selectById(aid);
        int curSigned = activity.getSignedNumber();

        if(activities != null){
            //说明已经报名了，就取消报名
            userActivityMapper.deleteById(activities.getId());
            activity.setSignedNumber(curSigned-1);

        }else {
            //否则，就新增一项报名的活动
            UserActivity userActivity = UserActivity.builder().uid(uid).aid(aid).status(ActivityStatus.SIGNED_UP.toString()).build();
            userActivityMapper.insert(userActivity);
            activity.setSignedNumber(curSigned+1);
        }
        activityMapper.updateById(activity);
    };

    /**
     * 用户评论活动
     * @param aid：活动id
     * @param content:评论内容
     * @param uid:用户id
     * @param time：评论时间
     */
    public void commentActivity(Integer aid, String content, Integer uid, Date time){
        ActivityComment activityComment = ActivityComment.builder().aid(aid).uid(uid).content(content).time(time).build();
        activityCommentMapper.insert(activityComment);
    }

    /**
     * 首页请求展示活动内容后返回内容，与活动信息表有关
     * @param showStartNum:当前从数据库第几个开始返回
     * @return 活动列表
     */
    public List<ActivityBriefVo> showAllActivity(int showStartNum){
        List<ActivityBriefVo> list = new ArrayList<>();
        List<Activity> activities = activityMapper.findAll();

       for(int i = 0; i < activities.size(); i++){
            Activity activity = activities.get(i);

            User user = userMapper.selectById(activity.getUid());
            ActivityBriefVo activityBriefVo = new ActivityBriefVo();
           LocalDateTime currentDateTime = LocalDateTime.now();
           // 转换为时间戳
           Instant instant = currentDateTime.atZone(ZoneId.systemDefault()).toInstant();
           long timestamp = instant.toEpochMilli();
           if(timestamp < activity.getStartSign().getTime()){
               activityBriefVo.setProcess(ActivityProcess.NOT_STARTED.getProcess());
           } else if(timestamp < activity.getEndSign().getTime()){
               activityBriefVo.setProcess(ActivityProcess.REGISTRATION_OPEN.getProcess());
           } else if(timestamp > activity.getEndSign().getTime()  && timestamp <activity.getStartTime().getTime()){
               activityBriefVo.setProcess(ActivityProcess.REGISTRATION_CLOSED.getProcess());
           } else if(timestamp < activity.getEndTime().getTime()){
               activityBriefVo.setProcess(ActivityProcess.ONGOING.getProcess());
           } else{
               activityBriefVo.setProcess(ActivityProcess.FINISHED.getProcess());
           }
            activityBriefVo.setId(activity.getId());
            activityBriefVo.setName(activity.getName());
            activityBriefVo.setImg(activity.getImg());
            if(user != null){
                activityBriefVo.setUsername(user.getName());
                activityBriefVo.setUserImg(user.getAvatarUrl());
            }
            activityBriefVo.setSignedNumber(activity.getSignedNumber());
            activityBriefVo.setPraisedNumber(activity.getPraisedNumber());
            activityBriefVo.setMaxNumber(activity.getMaxNumber());
            list.add(activityBriefVo);
        }

        return list;

    }



    /**
     * 用户点击详情页面用于查看Activity详情，因此需要包含活动信息表内容和用户-活动表
     * @param aid:活动具体的id
     * @return 返回具体活动信息
     */
    public ActivityVo showDetailActivity(Integer aid){
        List<Activity> activities = activityMapper.findAll();
        for(Activity activity: activities){
            if(activity.getId().equals(aid)){
                return convertVo(activity);
            }
        }
        System.out.println("return null in show detail Activity");
        return null;
    }

    public List<CommentVo> showComment(Integer aid){
        List<ActivityComment> activityComments = activityCommentMapper.findAll();
        List<CommentVo> comments = new ArrayList<>();
        for(int i = 0; i < activityComments.size(); i++){
            if(activityComments.get(i).getAid().equals(aid)){
                CommentVo commentVo = new CommentVo();
                commentVo.setContent(activityComments.get(i).getContent());
                commentVo.setUid(activityComments.get(i).getUid());
                commentVo.setTime(activityComments.get(i).getTime());
                comments.add(commentVo);
            }
        }
        return comments;
    }

    public List<ActivityVo> search(String searchInfo){
        List<Activity> activities = activityMapper.findAll();
        List<ActivityVo> searchResults = new ArrayList<>();
        for(int i = 0; i < activities.size(); i++){
            String detailInfo = activities.get(i).getName()+activities.get(i).getIntroduction();
            int index = detailInfo.indexOf(searchInfo);
            if(index != -1){ //在活动标题或介绍里查找到了
                Activity activity = activities.get(i);
                ActivityVo activityVo = convertVo(activity);
                searchResults.add(activityVo);
            }
            else {
                List<ActivityLabel> labels = activityLabelMapper.findAll();
                for(int j = 0; j < labels.size(); j++){
                    ActivityLabel label = labels.get(j);
                    if(label.getAid().equals(activities.get(i).getId())){
                        int label_index = label.getLabel().indexOf(searchInfo);
                        if(label_index != -1){
                            Activity activity = activities.get(i);
                            ActivityVo activityVo = convertVo(activity);
                            searchResults.add(activityVo);
                            break;
                        }
                    }
                }
            }
        }

        return searchResults;
    }


    public void delete(Integer id){

    }
}

