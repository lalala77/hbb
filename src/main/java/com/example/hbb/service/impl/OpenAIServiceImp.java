package com.example.hbb.service.impl;

import com.example.hbb.model.MessageTypes;
import com.example.hbb.service.MessageService;
import com.example.hbb.service.OpenAIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Liululin
 * @date 2024/1/14 - 7:44
 */
@Service
public class OpenAIServiceImp implements OpenAIService {
    @Autowired
    MessageService messageService;
    //todo
    @Override
    public String getFeedback(Integer uid, String title, String introduction) {
        boolean containsSensitiveContent = checkForSensitiveContent(title) || checkForSensitiveContent(introduction);
        if (containsSensitiveContent) {
            messageService.addMessage(uid, MessageTypes.PUBLISH_FAIL , title + "&" + "该内容涉及政治敏感信息。");
            return "该内容涉及政治敏感信息。";
        } else {
            try {
                Thread.sleep(10000); // 等待10秒
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            messageService.addMessage(uid, MessageTypes.PUBLISH_SUCCESS , title);
            return "您已成功发布"+title+"活动，请在个人中心查看";
        }
    }

    private boolean checkForSensitiveContent(String content) {
        // 定义政治敏感词列表
        String[] sensitiveWords = {
                "政府", "党派", "选举", "领导", "抗议",
                 "起义", "反抗国家"
        };

        // 将内容转换为小写，便于匹配
        content = content.toLowerCase();

        // 检查关键词是否出现在内容中
        for (String word : sensitiveWords) {
            if (content.contains(word.toLowerCase())) {
                return true; // 包含敏感词
            }
        }

        return false; // 不包含敏感词
    }
}
