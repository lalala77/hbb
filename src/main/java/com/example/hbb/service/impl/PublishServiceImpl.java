package com.example.hbb.service.impl;

import com.example.hbb.dao.ActivityLabelMapper;
import com.example.hbb.dao.ActivityMapper;
import com.example.hbb.model.po.Activity;
import com.example.hbb.model.po.ActivityLabel;
import com.example.hbb.service.OpenAIService;
import com.example.hbb.service.PublishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
@Service
public class PublishServiceImpl implements PublishService {
    @Autowired
    ActivityMapper activityMapper;

    @Autowired
    ActivityLabelMapper activityLabelMapper;

    @Autowired
    OpenAIService openAIService;
    @Override
    public int publishActivity(Integer uid, String name, String introduction, String img, Date startTime, Date endTime, Date startSign, Date endSign, Integer maxNumber) {
        Activity activity = new Activity();
        activity.setUid(uid);
        activity.setName(name);
        activity.setIntroduction(introduction);
        activity.setImg(img);
        activity.setStartTime(startTime);
        activity.setEndTime(endTime);
        activity.setStartSign(startSign);
        activity.setEndSign(endSign);
        activity.setMaxNumber(maxNumber);
        activity.setStatus(-1);
        activity.setSignedNumber(0);

        String feedback = openAIService.getFeedback(uid,name, introduction);
        // todo 向用户推送消息

        // 调用 ActivityMapper 插入数据库的方法
        if(!feedback.contains("涉及")) {
            activityMapper.insert(activity);
        }
        // 返回插入结果

        return activity.getId();
    }

    @Override
    public void addActivityLabel(Integer id, ArrayList<String> labels) {
        for(String label:labels){
            ActivityLabel activityLabel = new ActivityLabel();
            activityLabel.setAid(id);
            activityLabel.setLabel(label);
            activityLabelMapper.insert(activityLabel);
        }
    }

    @Override
    public void editActivity(Integer id, String name, String introduction,String img, Date startTime, Date endTime, Date startSign, Date endSign, Integer maxNumber) {
        Activity activity = activityMapper.selectById(id);
        activity.setName(name);
        activity.setIntroduction(introduction);
        activity.setImg(img);
        activity.setStartTime(startTime);
        activity.setEndTime(endTime);
        activity.setStartSign(startSign);
        activity.setEndSign(endSign);
        activity.setMaxNumber(maxNumber);
        activityMapper.updateById(activity);
    }
}
