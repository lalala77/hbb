package com.example.hbb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.hbb.dao.MessageMapper;
import com.example.hbb.model.MessageTypes;
import com.example.hbb.model.po.Activity;
import com.example.hbb.model.po.Message;
import com.example.hbb.model.po.UserActivity;
import com.example.hbb.model.vo.ActivityVo;
import com.example.hbb.model.vo.MessageVo;
import com.example.hbb.service.ActivityService;
import com.example.hbb.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.util.ArrayList;
import java.util.List;

@Service
@Component
@EnableAsync
public class MessageServiceImpl implements MessageService {
    @Autowired
    MessageMapper messageMapper;
    @Autowired
    ActivityService activityService;


    public void judgeDue(Integer uid, ArrayList<Integer> activities){
        //获取到当前用户所有报名的活动id，轮询
        LocalDate date = LocalDate.now(); // get the current date

        // 计算两个日期之间的差距
        for(Integer id : activities){
            ActivityVo activityVo = activityService.showDetailActivity(id);
            Instant instant = activityVo.getStartTime().toInstant();
            ZoneId zoneId = ZoneId.systemDefault();
            // atZone()方法返回在指定时区从此Instant生成的ZonedDateTime。
            LocalDate startDate = instant.atZone(zoneId).toLocalDate();

            long daysCollect = ChronoUnit.DAYS.between( date, startDate);
            if(daysCollect < 2){
                addMessage(uid, MessageTypes.ACTIVITY_DUE,activityVo.getName());
            }
        }
    }

    public void addMessage(Integer uid, MessageTypes type, String content){
        LocalDate date = LocalDate.now(); // get the current date
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        Message message = new Message();
        message.setType(type.toString());
        message.setUid(uid);
        message.setContent(content);
        message.setDate(date.format(formatter));
        messageMapper.insert(message);
    }

    public ArrayList<MessageVo> getAllMessages(int userId){
        //where条件
        QueryWrapper<Message> messageQueryWrapper = new QueryWrapper<>();
        messageQueryWrapper .eq("uid", userId);

        List<Message> messages = messageMapper.selectList(messageQueryWrapper);

        ArrayList<MessageVo> res = new ArrayList<>();

        for(Message message : messages){
            res.add(new MessageVo(message));
        }
        return res;
    }
}
