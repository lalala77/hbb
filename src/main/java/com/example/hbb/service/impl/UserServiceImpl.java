package com.example.hbb.service.impl;

import java.awt.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.hbb.dao.UserActivityMapper;
import com.example.hbb.dao.UserMapper;
import com.example.hbb.model.ActivityStatus;
import com.example.hbb.model.exception.BizError;
import com.example.hbb.model.exception.BizException;
import com.example.hbb.model.po.Message;
import com.example.hbb.model.po.User;
import com.example.hbb.model.po.UserActivity;
import com.example.hbb.model.vo.MessageVo;
import com.example.hbb.model.vo.UserVo;
import com.example.hbb.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.hbb.model.exception.BizError.USER_NOEXISTS;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;
    @Autowired
    UserActivityMapper userActivityMapper;

    public static String passwordEncoder(String inputStr)
    {
        BigInteger sha =null;
//        System.out.println("=======加密前的数据:"+inputStr);
        byte[] inputData = inputStr.getBytes();
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA");
            messageDigest.update(inputData);
            sha = new BigInteger(messageDigest.digest());
//            System.out.println("SHA加密后:" + sha.toString(32));
        } catch (Exception e) {e.printStackTrace();}
        return sha.toString(32);
    }


    public void addUser(String sid, String userName, String password, String avatarUrl){
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("sid", sid);
        if(!userMapper.exists(userQueryWrapper)){
            User user = new User();
            user.setPassword(passwordEncoder(password));
            user.setSid(sid);
            user.setName(userName);
            user.setAvatarUrl(avatarUrl);
            userMapper.insert(user);
        }else {
            throw new BizException(BizError.USER_EXISTS);
        }
    }

    /**
     * 实现用户的登录
     * @param sId：用户学号
     * @param password：用户密码
     * return: 用户id
     */

    @Override
    public Integer login(String sId, String password) {
        System.out.println(sId);
        System.out.println(password);
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("sid", sId);
        User user = userMapper.selectOne(userQueryWrapper);
        if( user == null || !passwordEncoder(password).equals(user.getPassword())){
            throw new BizException(BizError.INVALID_CREDENTIAL);
        }
        return user.getId();
    }

    /**
     * 修改用户的昵称
     * @param Id：用户ID
     * @param newName：用户新的名字
     */
    public void editUsername(Integer Id, String newName) {
        User user = userMapper.selectById(Id);
        user.setName(newName);
        userMapper.updateById(user);
    }

    /**
     * 修改用户头像
     * @param Id：用户ID
     * @param newAvatarUrl：统一资源符
     */
    public void editUserImg(Integer Id, String newAvatarUrl){
        User user = userMapper.selectById(Id);
        user.setAvatarUrl(newAvatarUrl);
        userMapper.updateById(user);
    }

    //实现返回UserVo
    public UserVo getUserVo(Integer userId){
        User user = userMapper.selectById(userId);
        if(user == null){
            throw new BizException(BizError.USER_NOEXISTS);
        }
        UserVo userVo = new UserVo(user);

        // 设置收藏列表、发布列表、参与列表
        ArrayList<Integer> collects = new ArrayList<>();
        ArrayList<Integer> signUps = new ArrayList<>();

        Map<String,String> publish = new HashMap<>();

        //where条件
        QueryWrapper<UserActivity> userActivityQueryWrapper = new QueryWrapper<>();
        userActivityQueryWrapper.eq("uId", userId);

        List<UserActivity> activitis = userActivityMapper.selectList(userActivityQueryWrapper);
        for(UserActivity activity : activitis){
            ActivityStatus status = ActivityStatus.valueOf(activity.getStatus());
            // 普通用户视角下活动的状态 1.待审核 2.发布成功 3.已收藏 4。已报名
            switch (status) {
                case FAVORITE -> collects.add(activity.getAid());
                case SIGNED_UP -> signUps.add(activity.getAid());
                default -> publish.put(activity.getAid().toString(), status.getStatus());
            }
        }
        userVo.setCollectActs(collects);
        userVo.setSignUpActs(signUps);
        userVo.setPublishActs(publish);

        return userVo;
    }

    public ArrayList<Integer> getAllUsers(){
        return userMapper.getAllIds();
    }




}
