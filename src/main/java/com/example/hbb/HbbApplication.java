package com.example.hbb;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
//@MapperScan("com.*.*.dao")
//@ComponentScan("com.*.*.*")
//@MapperScan({"com.gitee.sunchenbin.mybatis.actable.dao.*"})//固定的
//@ComponentScan("com.gitee.sunchenbin.mybatis.actable.manager.*")//固定的
public class HbbApplication {

	public static void main(String[] args) {
		SpringApplication.run(HbbApplication.class, args);
	}

}
