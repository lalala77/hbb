package com.example.hbb.web.controller;

/**
 * @author Liululin
 * @date 2024/1/2 - 10:33
 */

import com.example.hbb.model.MessageTypes;
import com.example.hbb.model.vo.MessageVo;
import com.example.hbb.model.vo.UserVo;
import com.example.hbb.model.vo.user.EditImgRequest;
import com.example.hbb.model.vo.user.EditNameRequest;
import com.example.hbb.model.vo.user.LoginRequest;
import com.example.hbb.model.vo.user.RegisterRequest;
import com.example.hbb.service.MessageService;
import com.example.hbb.service.UserService;
import com.example.hbb.web.controller.response.Response;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;

/**
 * 负责处理用户登录、身份认证和信息修改的业务
 */

@RestController
@RequestMapping("/user/")
@RequiredArgsConstructor
public class userController {
    private final UserService userService;
    private final MessageService messageService;
    @PostMapping("login")
    public Response login(@Valid @RequestBody LoginRequest request){
        System.out.println("login now");
        int userId = userService.login(request.getUserId(),request.getPassword());
        return Response.loginSuccess(userId);
    }

    @PostMapping("editName")
    public Response editName(@Valid @RequestBody EditNameRequest editNameRequest){
        userService.editUsername(editNameRequest.getId(), editNameRequest.getName());
        return Response.editSuccess();
    }

    @PostMapping("editImg")
    public Response editImg(@Valid @RequestBody EditImgRequest editImgRequest){
        userService.editUserImg(editImgRequest.getId(), editImgRequest.getAvatarUrl());
        return Response.editSuccess();
    }

    @PutMapping("registry")
    public Response addUser(@Valid @RequestBody RegisterRequest registerRequest){
        //用户注册，测试用
        userService.addUser(registerRequest.getUserId(), registerRequest.getName(), registerRequest.getPassword(), registerRequest.getAvatarUrl());
        return Response.editSuccess();
    }

    @GetMapping("{id}")
    public UserVo getUser(@PathVariable Integer id){
        UserVo userVo = userService.getUserVo(id);
        messageService.judgeDue(id, userVo.getSignUpActs());
        return userService.getUserVo(id);
    }

    @GetMapping
    public ArrayList<Integer> getAllUser(){
        return userService.getAllUsers();
    }

    @GetMapping("message/{userId}")
    public ArrayList<MessageVo> getMessages(@PathVariable Integer userId){
//        messageService.addMessage(6, MessageTypes.PUBLISH_SUCCESS, "哈哈哈");
//        messageService.addMessage(6, MessageTypes.PUBLISH_FAIL, "游行 & 不符合国家规定");
        return messageService.getAllMessages(userId);
    }


}
