package com.example.hbb.web.controller;

/**
 * @author Liululin
 * @date 2024/1/2 - 10:35
 */

import com.example.hbb.model.vo.activity.CollectRequest;
import com.example.hbb.model.vo.activity.CommentRequest;
import com.example.hbb.model.vo.activity.SignUpRequest;
import com.example.hbb.service.ActivityService;
import com.example.hbb.web.controller.response.Response;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Date;

/**
 * 负责实现用户报名活动、评价活动、查看活动所需要的业务
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/activity/")
public class activityController {
    private final ActivityService activityService;
    private int currentPresentNum = -6; // 对于首页请求展示活动，每次请求只返回6个活动，逐渐往下返回
    @PostMapping("signup")
    public Response signUpActivity(@Valid @RequestParam Integer uid, Integer aid){
        activityService.signUpActivity(uid,aid);
        return Response.signUpSuccess();
    }

    @PostMapping("collect")
    public Response collectActivity(@Valid @RequestParam Integer uid, Integer aid){
        activityService.collectActivity(uid,aid);
        return Response.collectSuccess();
    }

    @PostMapping("comment")
    public Response commentActivity(@Valid @RequestParam Integer aid, String content, Integer uid){
        Date date = new Date();
        activityService.commentActivity(aid,content,uid, date);
        return Response.commentSuccess();
    }

    @GetMapping("show")
    public Response showAllActivity(){
        currentPresentNum += 6; //如果第一次，从0开始展示
        return Response.showAllSuccess(activityService.showAllActivity(currentPresentNum));
    }

    @GetMapping("showDetail")
    public Response showDetailActivity(@RequestParam Integer aid){
        System.out.println("get specific detail");
        return Response.showDetailSuccess(activityService.showDetailActivity(aid));
    }

    @GetMapping("comment")
    public Response showComment(@RequestParam Integer aid){
        return Response.showCommentSuccess(activityService.showComment(aid));
    }

    @GetMapping("search")
    public Response search(@RequestParam String searchInfo){
        return Response.showCommentSuccess(activityService.search(searchInfo));
    }
    //仅测试用
    @DeleteMapping("delete/{id}")
    public Response delete(@PathVariable Integer id){
        activityService.delete(id);
        return Response.editSuccess();
    }
}
