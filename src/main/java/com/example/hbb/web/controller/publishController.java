package com.example.hbb.web.controller;

/**
 * @author GuoJiangYun
 * @date 2024/1/2 - 10:34
 */
import com.example.hbb.model.vo.activity.EditActRequest;
import com.example.hbb.model.vo.activity.PublishActRequest;
import com.example.hbb.service.PublishService;
import com.example.hbb.web.controller.response.Response;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 负责用户活动的发布、发布后对活动的修改
 */
@RestController
@RequestMapping("/publish/")
@RequiredArgsConstructor
public class publishController {
    private final PublishService publishService;

    @PostMapping("new")
    public Response publishNewActivity(@Valid @RequestBody PublishActRequest publishActRequest){
        int activityId = publishService.publishActivity(publishActRequest.getUid(), publishActRequest.getName(), publishActRequest.getIntroduction(),publishActRequest.getImg(),
                publishActRequest.getStartTime(),publishActRequest.getEndTime(),publishActRequest.getStartSign(),
                publishActRequest.getEndSign(),publishActRequest.getMaxNumber());
        return Response.publishSuccess();
    }

    @PutMapping("edit")
    public Response editActivity(@Valid @RequestBody EditActRequest editActRequest){
        publishService.editActivity(editActRequest.getId(),editActRequest.getName(), editActRequest.getIntroduction(),editActRequest.getImg(),
                editActRequest.getStartTime(), editActRequest.getEndTime(),editActRequest.getStartSign(),editActRequest.getEndSign(),
                editActRequest.getMaxNumber());
        return Response.editActSuccess();
    }



}
