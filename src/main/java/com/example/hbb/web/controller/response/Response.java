package com.example.hbb.web.controller.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Response {
    private String code;
    private String msg;
    private Object result;

    public Response(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public static Response buildSuccess(Object result) {
        return new Response("00000", "Success", result);
    }

    public static Response buildSuccess() {return new Response("00000", "Success", "操作成功");};

    public static Response buildFailed(String code, RuntimeException e) {
        return new Response(code, e.getLocalizedMessage(), null);
    }

    public static Response loginSuccess(int userId){return new Response("00001","LoginSuccess",userId);}
    public static Response editSuccess(){return new Response("00002","EditSuccess","修改信息成功");}

    public static Response publishSuccess(){return new Response("00010","PublishSuccess","发布活动成功");}
    public static Response editActSuccess(){return new Response("00011","EditActSuccess","修改活动成功");}

    public static Response signUpSuccess(){return new Response("00020","SignUpSuccess","报名成功");}
    public static Response collectSuccess(){return new Response("00021","CollectSuccess","收藏成功");}
    public static Response commentSuccess(){return new Response("00022","CommentSuccess","评论成功");}

    public static Response showAllSuccess(Object result){return new Response("00030","ShowAllSuccess",result);}
    public static Response showDetailSuccess(Object result){return new Response("00031","ShowDetailSuccess",result);}
    public static Response showCommentSuccess(Object result){return new Response("00032","ShowCommentSuccess",result);}

    public static Response buildFailed(String code, String msg) {
        return new Response(code, msg, null);
    }
}
